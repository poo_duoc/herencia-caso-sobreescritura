/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package herencia;

/**
 *
 * @author Palto
 */
public class Main {
    public static void main(String[]args){
            
        Animal generico = new Animal();
        Animal can = new Can();
        Animal gato = new Gato();
        
        Animal [] animales = {generico, can, gato};
        
        for(Animal aux : animales){
            aux.comunicar();
        }
       
    }

}
